<?php

namespace App\Repository;

use App\Entity\CustomView;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CustomView|null find($id, $lockMode = null, $lockVersion = null)
 * @method CustomView|null findOneBy(array $criteria, array $orderBy = null)
 * @method CustomView[]    findAll()
 * @method CustomView[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomViewRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CustomView::class);
    }

    // /**
    //  * @return CustomView[] Returns an array of CustomView objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?CustomView
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
