<?php
namespace App\Admin;


use App\Entity\Category;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

final class ProductAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $form) : void
    {
        $form
            ->add('name',TextType::class)
            ->add('createAt',DateTimeType::class)
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'placeholder' => 'Choice Category',
                'attr' => [
                   # 'select2-focusser select2-offscreen' => 'false',
                    'data-sonata-select2-minimumResultsForSearch' => '3'
                ]
            ))
            ->end();

    }

    protected function configureDatagridFilters(DatagridMapper $filter) : void
    {
        $filter
            ->add('name')
            ->add('createAt')
            ->add('category');
    }

    protected function configureListFields(ListMapper $list) : void
    {
        $list
            ->addIdentifier('name')
            ->addIdentifier('createAt')
            ->addIdentifier('category');
    }
}
