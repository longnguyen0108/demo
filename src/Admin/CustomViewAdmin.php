<?php
namespace App\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

final class CustomViewAdmin extends AbstractAdmin
{
    protected $baseRouteName = 'admin';
    protected $baseRoutePattern = 'admin';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list'));
    }
}
