<?php

namespace App\Controller;


use App\Entity\Category;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;

class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product_list")
     * @Method({"GET"})
     */
    public function index() {

        $products= $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', array('products' => $products));
    }

    /**
     * @Route("/product/new", name="new_product")
     * Method({"GET", "POST"})
     */
    public function new(Request $request) {
        $product = new Product();

        $form = $this->createFormBuilder($product)
                     ->add('name', TextType::class)
                     ->add('createAt', DateTimeType::class)
                    ->add('category', EntityType::class, array(
                        'class' => Category::class,
                        'placeholder' => 'no rights'
                    ))
                     ->add('save', SubmitType::class, array(
                         'label' => 'Create'
                     ))

                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $product = $form->getData();

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/new.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/product/edit/{id}", name="edit_product")
     * Method({"GET", "POST"})
     */
    public function edit(Request $request, $id) {
        $product = new Product();
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $form = $this->createFormBuilder($product)
                    ->add('name', TextType::class)
                    ->add('createAt', DateTimeType::class)
                    ->add('category', EntityType::class, array(
                        'class' => Category::class,
                        'placeholder' => 'no rights'
                    ))
                    ->add('save', SubmitType::class, array(
                        'label' => 'Update'
                    ))
                     ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();

            return $this->redirectToRoute('product_list');
        }

        return $this->render('product/edit.html.twig', array(
            'form' => $form->createView()
        ));
    }

    /**
     * @Route("/product/{id}", name="show_product")
     */
    public function show($id) {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        return $this->render('product/show.html.twig', array('product' => $product));
    }

    /**
     * @Route("/product/delete/{id}")
     * @Method({"DELETE"})
     */
    public function delete(Request $request, $id) {
        $product = $this->getDoctrine()->getRepository(Product::class)->find($id);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($product);
        $entityManager->flush();

//        $response = new Response();
//        $response->send();

        $products= $this->getDoctrine()->getRepository(Product::class)->findAll();

        return $this->render('product/index.html.twig', array('products' => $products));
    }
//    /**
//     * @Route("/product", name="product")
//     */
//
//    public function createProduct(): Response
//    {
//        $product = new Product();
//        $product->setName('Car');
//        $product->setCreateAt(new \DateTime('@'.strtotime('now')));
//
//        $category = new Category();
//        $category->setName('Chair');
//
//        $product->setCategory($category);
//
//        $entityManager = $this->getDoctrine()->getManager();
//        $entityManager->persist($category);
//        $entityManager->persist($product);
//        $entityManager->flush();
//
//        return new Response(
//            'Saved new product with id: '.$product->getId()
//            .' and new category with id: '.$category->getId()
//        );
//    }
//
//    public function index(): Response
//    {
//        return $this->render('product/index.html.twig', [
//            'controller_name' => 'ProductController',
//        ]);
//    }
}
