<?php

// src/Controller/CarAdminController.php

namespace App\Controller;

use Sonata\AdminBundle\Controller\CRUDController;

class CustomViewCRUDController extends CRUDController
{
    public function listAction()
    {
        return $this->render('custom_view_crud/index.html.twig');
    }
}