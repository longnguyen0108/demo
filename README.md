git clone this project

cd into the project folder

update DATABASE_URL in .env or in your .env.local

composer install

php bin/console doctrine:database:create

php bin/console make:migration

php bin/console doctrine:migrations:migrate

php bin/console fos:user:create adminuser --super-admin (create admin user)

symfony server:start
